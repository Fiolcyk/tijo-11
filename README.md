## Singleton (wzorzec projektowy)

Prototyp – kreacyjny wzorzec projektowy, którego celem jest umożliwienie
tworzenia obiektów danej klasy bądź klas z wykorzystaniem już istniejącego 
obiektu, zwanego prototypem. Głównym celem tego wzorca jest uniezależnienie 
systemu od sposobu w jaki tworzone są w nim produkty

## Przykład zastosowania

Omawiany wzorzec stosujemy między innymi wtedy, gdy nie chcemy tworzyć w budowanej 
aplikacji podklas obiektu budującego (jak to jest w przypadku wzorca fabryki abstrakcyjnej). 
Wzorzec ten stosujemy podczas stosowania klas specyfikowanych 
podczas działania aplikacji


## Szczegóły implementacyjne

Wzorzec prototypu określa rodzaj obiektów do tworzenia 
za pomocą prototypowej instancji. Prototypy nowych produktów 
są często budowane przed pełną produkcją, ale w tym 
przykładzie, prototyp jest bierny i nie bierze udziału 
w kopiowaniu siebie samego. Przykładem aktywnego prototypu 
(czyli biorącego udział w kopiowaniu siebie samego) 
jest biologiczny podział jednej komórki w dwie identyczne. 
 Wtedy mamy do czynienia z klonowaniem.

![implementacja](https://www.tutorialspoint.com/design_pattern/images/prototype_pattern_uml_diagram.jpg)
## Praktyczne wskazówki

Czasem wzorce konstrukcyjne pokrywają się. Są przypadki, gdzie odpowiedni 
byłby zarówno Prototyp, jak i Fabryka Abstrakcyjna. W innych przypadkach 
te wzorce nawzajem się uzupełniają: Fabryka abstrakcyjna mogłaby przechowywać 
zbiór Prototypów, które mogłaby klonować i zwracać jako obiekt produktu. 
Fabryka abstrakcyjna, Budowniczy i Prototyp mogą w swych implementacjach używać 
wzorca Singletonu. Klasy Fabryki abstrakcyjnej są często implementowane 
razem z Metodami fabrykującymi (tworzenie przez dziedziczenie), ale mogą być też 
implementowane przy użyciu Prototypu (tworzenie przez delegację).

Często projekty startują wraz z użyciem Metod fabrykujących 
(które są mniej skomplikowane, bardziej elastyczne i z szybko powiększającą się 
liczbą podklas) i ewoluują w kierunku Fabryki abstrakcyjnej, Prototypu lub 
Budowniczego (o większych możliwościach i bardziej złożonych) wtedy, gdy projektant 
uzmysłowi sobie potrzebę stosowania rozwiązań o większej mocy.

Prototypy nie wymagają "podklasowania", ale wymagają operacji inicjalizacji. 
Metody fabrykujące wymagają "podklasowania" ale nie wymagają inicjalizacji.

Projekty, które intensywnie używają wzorca Kompozytu i Dekoratora, mogą równie 
dobrze odnieść korzyść ze stosowania Prototypu.

Praktyczną wskazówką na to, kiedy może być potrzebnym używanie metody 
clone(), jest konieczność tworzenia prawdziwej kopii (ang. true copy) innej instancji 
w czasie wykonywania programu. Prawdziwa kopia to taka, w której skopiowany obiekt 
ma wszystkie swe pola identyczne z pierwowzorem. Gdy używa się 
operatora new, to wtedy te pola mają wartości początkowe. Przykładowo, jeśli 
projektuje się system do przeprowadzania transakcji bankowych, to wtedy potrzebna 
jest taka kopia obiektu, która przechowa dane konta, wykona transakcje na tej 
kopii i zamieni tę kopię z oryginałem. W takim przypadku potrzebna jest raczej metoda 
clone() niż operator new.


Źródło [Wikipedia](https://pl.wikipedia.org/wiki/Prototyp_(wzorzec_projektowy))